/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utn.programacion2.interfaz;

import javax.swing.JOptionPane;
import utn.programacion2.Utilitario.archivoSecuencial;
import utn.programacion2.tarea1s3.Deportivo;
import utn.programacion2.tarea1s3.Furgoneta;
import utn.programacion2.tarea1s3.Vehiculo;

/**
 *
 * @author User
 */
public class Editar extends javax.swing.JDialog {

    private String placaEditar;

    public Editar(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        this.inhabilitar();
        JOptionPane.showMessageDialog(this, "Digite el código de placa que desea ver o editar");
    }

    private void inhabilitar() {
        this.radFurgoneta.setSelected(true);
        this.tabCaracteristicasVehiculos.setEnabledAt(0, false);
        this.tabCaracteristicasVehiculos.setEnabledAt(1, false);
        this.spnCapacidad.setEnabled(false);
        this.txtPrecio.setEnabled(false);
        this.txtPotencia.setEnabled(false);
        this.cmbMarca.setEnabled(false);
        this.cmbColor.setEnabled(false);
        this.cmbDisponibilidad.setEnabled(false);
        this.cmbVelocidadMaxima.setEnabled(false);
        this.radDeportivo.setEnabled(false);
        this.radFurgoneta.setEnabled(false);
        this.btnEditar.setEnabled(false);
    }

    private void rehabilitar() {
        if (this.radFurgoneta.isSelected()) {
            this.tabCaracteristicasVehiculos.setEnabledAt(0, true);
        } else {
            this.tabCaracteristicasVehiculos.setEnabledAt(1, true);
        }
        this.spnCapacidad.setEnabled(true);
        this.txtPlaca.setEnabled(true);
        this.txtPlaca.setText(this.txtPlaca.getText().toUpperCase());
        this.txtPrecio.setEnabled(true);
        this.txtPotencia.setEnabled(true);
        this.cmbMarca.setEnabled(true);
        this.cmbColor.setEnabled(true);
        this.cmbDisponibilidad.setEnabled(true);
        this.cmbVelocidadMaxima.setEnabled(true);
        this.radDeportivo.setEnabled(true);
        this.radFurgoneta.setEnabled(true);
        this.btnEditar.setEnabled(true);
    }

    private String[] informacionPlaca(String pPlacaValidar) {
        boolean isRepetida = false;
        String[] informacionVehiculo = null;
        archivoSecuencial oArchivo = new archivoSecuencial("C://Users//User//Documents//NetBeansProjects//Tarea1S3//archivos", "Vehiculos.txt");
        String prueba = oArchivo.leer();
        if (!(oArchivo.leer().equals("")) && !(oArchivo.leer().equals("error"))) {
            String[] lineas = oArchivo.leerArray();
            for (int i = 0; i < lineas.length; i++) {
                String[] unaLinea = lineas[i].split(";");
                if (pPlacaValidar.equalsIgnoreCase(unaLinea[1])) {
                    informacionVehiculo = unaLinea;
                    this.placaEditar = pPlacaValidar;
                    this.rehabilitar();
                    this.txtPlaca.setEnabled(false);
                    this.txtPrecio.setEnabled(false);
                    isRepetida = true;
                    break;
                }
            }
            if (!isRepetida) {
                JOptionPane.showMessageDialog(this, "¡No se ha registrado la placa: " + pPlacaValidar.toUpperCase() + "!");
                this.limpiar();
                this.inhabilitar();
            }
        }
        return informacionVehiculo;
    }

    private boolean validarPlaca(String pPlaca) {
        boolean correcta = true;
        if (pPlaca.length() > 7) {
            JOptionPane.showMessageDialog(this, "¡La placa debe tener un formato de tres letras y tres números separados por guión medio (-), por ejemplo AAA-000!");
            correcta = false;
        } else {
            String primerosTresCaracteres = pPlaca.substring(0, 3);
            boolean caracteresEspeciales = false;
            for (int i = 0; i < primerosTresCaracteres.length(); i++) {
                caracteresEspeciales = validarVocalYCaracterEspecial(primerosTresCaracteres.charAt(i));
                if (caracteresEspeciales) {
                    break;
                }
            }
            if (caracteresEspeciales) {
                JOptionPane.showMessageDialog(this, "¡La placa debe tener un formato de tres letras y tres números separados por guión medio (-), por ejemplo AAA-000!");
                correcta = false;
            } else if (pPlaca.charAt(3) != '-') {
                JOptionPane.showMessageDialog(this, "¡La placa debe tener un formato de tres letras y tres números separados por guión medio (-), por ejemplo AAA-000!");
                correcta = false;
            } else {
                for (int i = 4; i < pPlaca.length(); i++) {
                    try {
                        int numeroValidar = Integer.parseInt("" + pPlaca.charAt(i));
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(this, "¡La placa debe tener un formato de tres letras y tres números separados por guión medio (-), por ejemplo AAA-000!");
                        correcta = false;
                    }
                }
            }
        }
        return correcta;
    }

    private boolean validarPrecio(String pPrecio) {
        boolean correcto = false;
        try {
            Double convertirPrecio = Double.parseDouble(pPrecio);
            correcto = true;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "¡El precio del vehículo debe ser un valor númerico!");
        }
        return correcto;
    }

    private boolean validarVocalYCaracterEspecial(char pCaracter) {
        boolean isEspecial = false;
        switch (pCaracter) {
            case '"':
            case '|':
            case '°':
            case '!':
            case '¡':
            case '¿':
            case '?':
            case '#':
            case '$':
            case '%':
            case '&':
            case '/':
            case '(':
            case ')':
            case '=':
            case '*':
            case '+':
            case '-':
            case '@':
            case '^':
            case '<':
            case '>':
            case '[':
            case ']':
            case '{':
            case '}':
            case '.':
            case ':':
            case ';':
            case '_':
            case '¬':
            case 'A':
            case 'E':
            case 'I':
            case 'O':
            case 'U':
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                isEspecial = true;
                break;
        }
        return isEspecial;
    }

    private boolean placaRepetida(String pPlacaValidar) {
        boolean isRepetida = false;
        archivoSecuencial oArchivo = new archivoSecuencial("C://Users//User//Documents//NetBeansProjects//Tarea1S3//archivos", "Vehiculos.txt");
        String prueba = oArchivo.leer();
        if (!(oArchivo.leer().equals("")) && !(oArchivo.leer().equals("error"))) {
            String[] lineas = oArchivo.leerArray();
            for (int i = 0; i < lineas.length; i++) {
                String[] unaLinea = lineas[i].split(";");
                if (pPlacaValidar.equals(unaLinea[1])) {
                    isRepetida = true;
                    JOptionPane.showMessageDialog(this, "¡La placa " + pPlacaValidar + " ya ha sido registrada, no se permite registrar vehículos con el mismo código de placa!");
                    break;
                }
            }
        }
        return isRepetida;
    }

    private String tipo(Vehiculo vehiculo) {
        return vehiculo.tipoVehiculo();
    }

    private void limpiar() {
        this.txtPlaca.setText("");
        this.txtPrecio.setText("");
        this.txtPotencia.setText("");
        this.cmbColor.setSelectedIndex(0);
        this.cmbMarca.setSelectedIndex(0);
        this.cmbDisponibilidad.setSelectedIndex(0);
        this.cmbVelocidadMaxima.setSelectedIndex(0);
        this.radFurgoneta.setSelected(true);
        this.spnCapacidad.setValue(2);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnsTipoVehiculo = new javax.swing.ButtonGroup();
        jpnTipoVehiculo = new javax.swing.JPanel();
        radFurgoneta = new javax.swing.JRadioButton();
        radDeportivo = new javax.swing.JRadioButton();
        lblRegistro = new javax.swing.JLabel();
        tabCaracteristicasVehiculos = new javax.swing.JTabbedPane();
        jpnFurgoneta = new javax.swing.JPanel();
        lblCapacidad = new javax.swing.JLabel();
        spnCapacidad = new javax.swing.JSpinner();
        lblDisponibilidad = new javax.swing.JLabel();
        cmbDisponibilidad = new javax.swing.JComboBox<>();
        jpnDeportivo = new javax.swing.JPanel();
        lblPotenciaDeportivo = new javax.swing.JLabel();
        txtPotencia = new javax.swing.JTextField();
        lblVelocidadMaxima = new javax.swing.JLabel();
        cmbVelocidadMaxima = new javax.swing.JComboBox<>();
        jpnInformacionGeneral = new javax.swing.JPanel();
        txtPlaca = new javax.swing.JTextField();
        lblPlaca = new javax.swing.JLabel();
        txtPrecio = new javax.swing.JTextField();
        lblPrecio = new javax.swing.JLabel();
        cmbMarca = new javax.swing.JComboBox<>();
        cmbColor = new javax.swing.JComboBox<>();
        lblMarca = new javax.swing.JLabel();
        lblColor = new javax.swing.JLabel();
        btnEditar = new javax.swing.JButton();
        btnBuscar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jpnTipoVehiculo.setBorder(javax.swing.BorderFactory.createTitledBorder("Tipo de vehículo"));

        btnsTipoVehiculo.add(radFurgoneta);
        radFurgoneta.setText("Furgoneta");
        radFurgoneta.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                radFurgonetaItemStateChanged(evt);
            }
        });

        btnsTipoVehiculo.add(radDeportivo);
        radDeportivo.setText("Deportivo");
        radDeportivo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                radDeportivoItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout jpnTipoVehiculoLayout = new javax.swing.GroupLayout(jpnTipoVehiculo);
        jpnTipoVehiculo.setLayout(jpnTipoVehiculoLayout);
        jpnTipoVehiculoLayout.setHorizontalGroup(
            jpnTipoVehiculoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnTipoVehiculoLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(radFurgoneta)
                .addGap(43, 43, 43)
                .addComponent(radDeportivo, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(13, Short.MAX_VALUE))
        );
        jpnTipoVehiculoLayout.setVerticalGroup(
            jpnTipoVehiculoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnTipoVehiculoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpnTipoVehiculoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(radFurgoneta)
                    .addComponent(radDeportivo))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        lblRegistro.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblRegistro.setText("Ver / Editar Vehículos");

        jpnFurgoneta.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lblCapacidad.setText("Capacidad");

        lblDisponibilidad.setText("Disponibilidad");

        cmbDisponibilidad.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Si", "No" }));

        javax.swing.GroupLayout jpnFurgonetaLayout = new javax.swing.GroupLayout(jpnFurgoneta);
        jpnFurgoneta.setLayout(jpnFurgonetaLayout);
        jpnFurgonetaLayout.setHorizontalGroup(
            jpnFurgonetaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnFurgonetaLayout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(jpnFurgonetaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblCapacidad)
                    .addComponent(spnCapacidad, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(73, 73, 73)
                .addGroup(jpnFurgonetaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cmbDisponibilidad, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblDisponibilidad))
                .addContainerGap(32, Short.MAX_VALUE))
        );
        jpnFurgonetaLayout.setVerticalGroup(
            jpnFurgonetaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnFurgonetaLayout.createSequentialGroup()
                .addContainerGap(25, Short.MAX_VALUE)
                .addGroup(jpnFurgonetaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblCapacidad)
                    .addComponent(lblDisponibilidad))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpnFurgonetaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(spnCapacidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbDisponibilidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(34, 34, 34))
        );

        tabCaracteristicasVehiculos.addTab("Furgoneta", jpnFurgoneta);

        jpnDeportivo.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lblPotenciaDeportivo.setText("Potencia");

        lblVelocidadMaxima.setText("Velocidad Máxima");

        cmbVelocidadMaxima.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "100", "150", "200" }));

        javax.swing.GroupLayout jpnDeportivoLayout = new javax.swing.GroupLayout(jpnDeportivo);
        jpnDeportivo.setLayout(jpnDeportivoLayout);
        jpnDeportivoLayout.setHorizontalGroup(
            jpnDeportivoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpnDeportivoLayout.createSequentialGroup()
                .addGroup(jpnDeportivoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpnDeportivoLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(txtPotencia, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jpnDeportivoLayout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addComponent(lblPotenciaDeportivo)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 45, Short.MAX_VALUE)
                .addGroup(jpnDeportivoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblVelocidadMaxima)
                    .addComponent(cmbVelocidadMaxima, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28))
        );
        jpnDeportivoLayout.setVerticalGroup(
            jpnDeportivoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnDeportivoLayout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(jpnDeportivoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPotenciaDeportivo)
                    .addComponent(lblVelocidadMaxima))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpnDeportivoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPotencia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbVelocidadMaxima, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(36, Short.MAX_VALUE))
        );

        tabCaracteristicasVehiculos.addTab("Deportivo", jpnDeportivo);

        jpnInformacionGeneral.setBorder(javax.swing.BorderFactory.createTitledBorder("Información general"));

        lblPlaca.setText("Placa:");

        lblPrecio.setText("Precio:");

        lblMarca.setText("Marca:");

        lblColor.setText("Color:");

        javax.swing.GroupLayout jpnInformacionGeneralLayout = new javax.swing.GroupLayout(jpnInformacionGeneral);
        jpnInformacionGeneral.setLayout(jpnInformacionGeneralLayout);
        jpnInformacionGeneralLayout.setHorizontalGroup(
            jpnInformacionGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnInformacionGeneralLayout.createSequentialGroup()
                .addGroup(jpnInformacionGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpnInformacionGeneralLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jpnInformacionGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(cmbMarca, javax.swing.GroupLayout.Alignment.LEADING, 0, 90, Short.MAX_VALUE)
                            .addComponent(txtPlaca, javax.swing.GroupLayout.Alignment.LEADING))
                        .addGap(39, 39, 39)
                        .addGroup(jpnInformacionGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtPrecio, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cmbColor, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jpnInformacionGeneralLayout.createSequentialGroup()
                        .addGap(39, 39, 39)
                        .addComponent(lblPlaca)
                        .addGap(100, 100, 100)
                        .addComponent(lblPrecio))
                    .addGroup(jpnInformacionGeneralLayout.createSequentialGroup()
                        .addGap(38, 38, 38)
                        .addComponent(lblMarca)
                        .addGap(100, 100, 100)
                        .addComponent(lblColor)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jpnInformacionGeneralLayout.setVerticalGroup(
            jpnInformacionGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnInformacionGeneralLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(jpnInformacionGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPlaca)
                    .addComponent(lblPrecio))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpnInformacionGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPlaca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPrecio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jpnInformacionGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblMarca)
                    .addComponent(lblColor))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jpnInformacionGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbMarca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbColor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(25, 25, 25))
        );

        btnEditar.setText("Editar");
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });

        btnBuscar.setText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tabCaracteristicasVehiculos, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jpnTipoVehiculo, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jpnInformacionGeneral, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(lblRegistro)
                .addGap(67, 67, 67))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(btnBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnEditar, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(50, 50, 50))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblRegistro)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jpnInformacionGeneral, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jpnTipoVehiculo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tabCaracteristicasVehiculos, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnEditar)
                    .addComponent(btnBuscar))
                .addContainerGap(18, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void radFurgonetaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_radFurgonetaItemStateChanged
        // TODO add your handling code here:
        if (this.radFurgoneta.isSelected()) {
            this.tabCaracteristicasVehiculos.setEnabledAt(0, true);
            this.tabCaracteristicasVehiculos.setSelectedIndex(0);
            this.tabCaracteristicasVehiculos.setEnabledAt(1, false);
        }
    }//GEN-LAST:event_radFurgonetaItemStateChanged

    private void radDeportivoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_radDeportivoItemStateChanged
        // TODO add your handling code here:
        if (this.radDeportivo.isSelected()) {
            this.tabCaracteristicasVehiculos.setEnabledAt(1, true);
            this.tabCaracteristicasVehiculos.setSelectedIndex(1);
            this.tabCaracteristicasVehiculos.setEnabledAt(0, false);
        }
    }//GEN-LAST:event_radDeportivoItemStateChanged

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        // TODO add your handling code here:
        if ((this.txtPlaca.getText().equals("")) | (this.txtPrecio.getText().equals(""))) {
            JOptionPane.showMessageDialog(this, "¡Debe ingresar los valores de Placa y Precio del vehículo!");
        } else if ((this.validarPlaca(this.txtPlaca.getText())) && (this.validarPrecio(this.txtPrecio.getText()))) {
            archivoSecuencial oArchivo = new archivoSecuencial("C://Users//User//Documents//NetBeansProjects//Tarea1S3//archivos", "Vehiculos.txt");
            String textoGuardar = "";
            if (this.radFurgoneta.isSelected()) {
                if (Integer.parseInt(this.spnCapacidad.getValue().toString()) < 2) {
                    JOptionPane.showMessageDialog(this, "¡La capacidad de la Furgoneta no puede ser menor a 2!");
                } else {
                    boolean disponibilidad = true;
                    if (this.cmbDisponibilidad.getSelectedIndex() == 1) {
                        disponibilidad = false;
                    }
                    Furgoneta oFurgoneta = new Furgoneta(Integer.parseInt(this.spnCapacidad.getValue().toString()), disponibilidad, this.cmbMarca.getSelectedItem().toString(), this.cmbColor.getSelectedItem().toString(), this.txtPlaca.getText().toUpperCase(), Double.parseDouble(this.txtPrecio.getText()));
                    String[] lineas = oArchivo.leerArray();
                    for (int i = 0; i < lineas.length; i++) {
                        String[] unaLinea = lineas[i].split(";");
                        if (this.placaEditar.equals(unaLinea[1])) {
                            textoGuardar += this.tipo(oFurgoneta) + ";" + oFurgoneta.getAtributos() + "\n";
                        } else {
                            textoGuardar += lineas[i] + "\n";
                        }
                    }
                    oArchivo.escribir(textoGuardar, "C://Users//User//Documents//NetBeansProjects//Tarea1S3//archivos", "Vehiculos.txt");
                    JOptionPane.showMessageDialog(this, "¡Se ha modificado el vehículo con placa: " + this.txtPlaca.getText().toUpperCase() + " correctamente!");
                    this.limpiar();
                    this.inhabilitar();
                    this.txtPlaca.setEnabled(true);
                }
            } else {
                if (this.txtPotencia.getText().equals("")) {
                    JOptionPane.showMessageDialog(this, "¡Debe ingresar un valor de Potencia!");
                } else {
                    Deportivo oDeportivo = new Deportivo(this.txtPotencia.getText(), this.cmbVelocidadMaxima.getSelectedItem().toString(), this.cmbMarca.getSelectedItem().toString(), this.cmbColor.getSelectedItem().toString(), this.txtPlaca.getText().toUpperCase(), Double.parseDouble(this.txtPrecio.getText()));
                    String[] lineas = oArchivo.leerArray();
                    for (int i = 0; i < lineas.length; i++) {
                        String[] unaLinea = lineas[i].split(";");
                        if (this.placaEditar.equals(unaLinea[1])) {
                            textoGuardar += this.tipo(oDeportivo) + ";" + oDeportivo.getAtributos() + "\n";
                        } else {
                            textoGuardar += lineas[i] + "\n";
                        }
                    }
                    oArchivo.escribir(textoGuardar, "C://Users//User//Documents//NetBeansProjects//Tarea1S3//archivos", "Vehiculos.txt");
                    JOptionPane.showMessageDialog(this, "¡Se ha modificado el vehículo con placa: " + this.txtPlaca.getText().toUpperCase() + " correctamente!");
                    this.limpiar();
                    this.inhabilitar();
                    this.txtPlaca.setEnabled(true);
                }
            }
        }
    }//GEN-LAST:event_btnEditarActionPerformed

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        // TODO add your handling code here:
        if (this.txtPlaca.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "¡Debe ingresar un código de placa!");
        } else {
            String[] infoPlaca = this.informacionPlaca(this.txtPlaca.getText().toUpperCase());
            if (!(infoPlaca == null)) {
                this.txtPrecio.setText(infoPlaca[4]);
                archivoSecuencial oArchivo = new archivoSecuencial("C://Users//User//Documents//NetBeansProjects//Tarea1S3//archivos", "Marcas.txt");
                int posicionIgual = 0;
                for (int i = 0; i < oArchivo.leerArray().length; i++) {
                    this.cmbMarca.addItem(oArchivo.leerArray()[i]);
                    if (this.cmbMarca.getItemAt(i).toString().equals(infoPlaca[2])) {
                        this.cmbMarca.setSelectedIndex(i);
                    }
                }
                oArchivo = new archivoSecuencial("C://Users//User//Documents//NetBeansProjects//Tarea1S3//archivos", "Colores.txt");
                for (int j = 0; j < oArchivo.leerArray().length; j++) {
                    this.cmbColor.addItem(oArchivo.leerArray()[j]);
                    if (this.cmbColor.getItemAt(j).toString().equals(infoPlaca[3])) {
                        this.cmbColor.setSelectedIndex(j);
                    }
                }
                if (infoPlaca[0].equals("Furgoneta")) {
                    this.radFurgoneta.setSelected(true);
                    this.spnCapacidad.setValue(Integer.parseInt(infoPlaca[5]));
                    if (infoPlaca[6].equals("true")) {
                        this.cmbDisponibilidad.setSelectedIndex(0);
                    } else {
                        this.cmbDisponibilidad.setSelectedIndex(1);
                    }
                } else {
                    this.radDeportivo.setSelected(true);
                    this.txtPotencia.setText(infoPlaca[5]);
                    switch (infoPlaca[6]) {
                        case "100":
                            this.cmbVelocidadMaxima.setSelectedIndex(0);
                            break;
                        case "150":
                            this.cmbVelocidadMaxima.setSelectedIndex(1);
                            break;
                        case "200":
                            this.cmbVelocidadMaxima.setSelectedIndex(2);
                            break;
                    }
                }
            }
        }
    }//GEN-LAST:event_btnBuscarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnEditar;
    private javax.swing.ButtonGroup btnsTipoVehiculo;
    private javax.swing.JComboBox<String> cmbColor;
    private javax.swing.JComboBox<String> cmbDisponibilidad;
    private javax.swing.JComboBox<String> cmbMarca;
    private javax.swing.JComboBox<String> cmbVelocidadMaxima;
    private javax.swing.JPanel jpnDeportivo;
    private javax.swing.JPanel jpnFurgoneta;
    private javax.swing.JPanel jpnInformacionGeneral;
    private javax.swing.JPanel jpnTipoVehiculo;
    private javax.swing.JLabel lblCapacidad;
    private javax.swing.JLabel lblColor;
    private javax.swing.JLabel lblDisponibilidad;
    private javax.swing.JLabel lblMarca;
    private javax.swing.JLabel lblPlaca;
    private javax.swing.JLabel lblPotenciaDeportivo;
    private javax.swing.JLabel lblPrecio;
    private javax.swing.JLabel lblRegistro;
    private javax.swing.JLabel lblVelocidadMaxima;
    private javax.swing.JRadioButton radDeportivo;
    private javax.swing.JRadioButton radFurgoneta;
    private javax.swing.JSpinner spnCapacidad;
    private javax.swing.JTabbedPane tabCaracteristicasVehiculos;
    private javax.swing.JTextField txtPlaca;
    private javax.swing.JTextField txtPotencia;
    private javax.swing.JTextField txtPrecio;
    // End of variables declaration//GEN-END:variables
}
